function addAnalytics () {
    if (typeof(ga) == "function") {
        ga('send', 'event',
        'Optimizely', 
        'Test 11a Buyers Picks', 
        'Variation 1', 0,
        {'nonInteraction': 1}
        );
    } 
    else {
        setTimeout(function () {
            addAnalytics();
        },50)
    }
}
addAnalytics();



  function addAnalytics () {
    if (typeof(gtag) == "function") {
        gtag('event', 'Optimizely', {
            'event_label': 'Layout Redesign',
            'event_category': 'Control',
            'non_interaction': true
          });
    } 
    else {
        setTimeout(function () {
            addAnalytics();
        },50)
    }
}
addAnalytics();