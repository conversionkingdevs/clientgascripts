function addHeapAnalytics () {
	if (typeof(heap) == "object" && heap.loaded) {
		heap.addUserProperties({'VWOTest-5-Rerun4-Audience': 'Original'}); // N = Test Number
	} 
	else {
        setTimeout(function () {
            addHeapAnalytics();
        },50);
     }
}
addHeapAnalytics();
