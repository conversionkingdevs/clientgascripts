function addAnalytics () {
	if (typeof(ga) == "function") {
        ga('send', {
            'hitType': 'event',         
            'eventCategory': 'VWO', 
            'eventAction': 'Test 6 Search Prominence',     
            'eventLabel': 'Original',
              'nonInteraction':1	
          });
	} 
	else {
        setTimeout(function () {
            addAnalytics();
        },50)
     }
}
addAnalytics();