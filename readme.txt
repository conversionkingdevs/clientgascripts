


/*** Read Me ***/

/** js Codes **/
GA = Google Analyitics
GT = Google Tag Manager
NF = Script Not Foundw

/** Inserting script **/

For clients with GA we add the script for the relevant client before making live. 

Optimizely script:
We place the script to activate as soon as the test has started.
Occasionally tests will have custom conditions to active from user activated events so remember to add it in.


Variables:
TEST NAME = to be retrieved from proworkflow
VARIATION = either "control" || variation_<variation number>

Code:
window.dataLayer = window.dataLayer || [];
dataLayer.push({ 
    'event': 'OptimizelyTrigger', 
    'OptimizelyCat': 'Optimizely', 
    'OptimizelyAct': 'TEST NAME', 
    'OptimizelyLab': 'VARIATION'
});
